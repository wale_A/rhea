using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RHEA;
using System;
using FluentAssertions;
using System.Threading.Tasks;

namespace RHEA.Test
{
    [TestClass]
    public class UnitTest1
    {
        private readonly Mock<IAccountService> _accountServiceMock;
        private readonly AccountInfo _subject;
        private readonly int _accountId;

        public UnitTest1()
        {
            this._accountServiceMock = new Mock<IAccountService>(MockBehavior.Strict);
            this._accountId = 1;
            this._subject = new AccountInfo(this._accountId, this._accountServiceMock.Object);
        }

        [TestMethod]
        public void CallToRefreshAmount_Calls_GetAccountAmount_InAccountService()
        {
            // Arrange
            this._accountServiceMock.Setup(x => x.GetAccountAmount(It.IsAny<int>())).Returns(Task.FromResult(It.IsAny<double>()));

            // Act
            Func<Task> act = async () => await this._subject.RefreshAmount();

            // Assert
            act.Should().NotThrow();
            this._accountServiceMock.Verify(x => x.GetAccountAmount(this._accountId), Times.Once);
        }
    }
}
